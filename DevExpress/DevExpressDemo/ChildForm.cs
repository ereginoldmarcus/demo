﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars.Ribbon;

namespace DevExpressDemo
{
    public partial class ChildForm : Form
    {
        public RibbonMiniToolbar Toolbar { get; set; }

        public ChildForm()
        {
            InitializeComponent();
        }

        private void panelControl1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                Toolbar.Show(PointToScreen(e.Location));
            }
        }
    }
}
