﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Utils.CodedUISupport;
using DevExpress.XtraBars.Docking;

namespace DevExpressDemo
{
    public partial class Form1 : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public Form1()
        {
            InitializeComponent();

            repositoryItemComboBox2.Items.AddRange(GetTaskList());

            var childForm = new ChildForm()
            {
                MdiParent = this,
                Toolbar = ribbonMiniToolbar1
            };

            childForm.Show();

        }

        private void barEditItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private static List<string> GetTaskList()
        {
            List<string> taskList = new List<string>();

            taskList.Add("/10 import");
            taskList.Add("/10 import/101 From SOSI");
            taskList.Add("/10 import/102 From DWG");
            taskList.Add("/20 Terrien");
            taskList.Add("/20 Terrien");
            taskList.Add("/20 Terrien");
            taskList.Add("/30 Exisiting Situation");
            taskList.Add("/40 Lines");

            return taskList;

        }

        private void LoadWindows()
        {
            
        }

        private void barButtonItem9_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //
        }

        private void barButtonItem10_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            MessageBox.Show($"Clicked on {e.Item.Name}");
        }

        private void barButtonItem14_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            dockManager1.AddPanel(DockingStyle.Right, pnlExplorer);
        }
    }
}
