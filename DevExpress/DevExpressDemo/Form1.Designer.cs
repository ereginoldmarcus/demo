﻿namespace DevExpressDemo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup1 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem1 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem2 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem3 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem4 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem5 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup2 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem6 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem7 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem8 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem9 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem10 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem11 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup3 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem12 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem13 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem14 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem15 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem16 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup4 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem17 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem18 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem19 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem20 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem21 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Node1");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Node2");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Node3");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Node4");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Node0", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3,
            treeNode4});
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("Node6");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("Node7");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("Node8");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("Node9");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("Node10");
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("Node5", new System.Windows.Forms.TreeNode[] {
            treeNode6,
            treeNode7,
            treeNode8,
            treeNode9,
            treeNode10});
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.applicationMenu1 = new DevExpress.XtraBars.Ribbon.ApplicationMenu(this.components);
            this.barButtonItem12 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem13 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.btnPan = new DevExpress.XtraBars.BarButtonItem();
            this.btnVisible = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.btnSelection = new DevExpress.XtraBars.BarButtonItem();
            this.btnWindow = new DevExpress.XtraBars.BarButtonItem();
            this.btnPoint = new DevExpress.XtraBars.BarButtonItem();
            this.btnPrevious = new DevExpress.XtraBars.BarButtonItem();
            this.btnNext = new DevExpress.XtraBars.BarButtonItem();
            this.btnZoomOut = new DevExpress.XtraBars.BarButtonItem();
            this.btnIn = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.barEditItem2 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.ribbonGalleryBarItem1 = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.barButtonItem9 = new DevExpress.XtraBars.BarButtonItem();
            this.galleryDropDown2 = new DevExpress.XtraBars.Ribbon.GalleryDropDown(this.components);
            this.barButtonItem10 = new DevExpress.XtraBars.BarButtonItem();
            this.skinRibbonGalleryBarItem1 = new DevExpress.XtraBars.SkinRibbonGalleryBarItem();
            this.galleryDropDown4 = new DevExpress.XtraBars.Ribbon.GalleryDropDown(this.components);
            this.barButtonItem11 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonMiniToolbar1 = new DevExpress.XtraBars.Ribbon.RibbonMiniToolbar(this.components);
            this.ribbonPageCategory1 = new DevExpress.XtraBars.Ribbon.RibbonPageCategory();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpHome = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgEdit = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgNavigation = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgMeasureTools = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgTasks = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpInsert = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpModeling = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpConstruction = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpView = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpOutput = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.repositoryItemPopupGalleryEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupGalleryEdit();
            this.repositoryItemDisplayFormat1 = new DevExpress.XtraRichEdit.Forms.Design.RepositoryItemDisplayFormat();
            this.galleryDropDown1 = new DevExpress.XtraBars.Ribbon.GalleryDropDown(this.components);
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.pnlSample = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel3_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.pnlProperties = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel5_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.propertyGridControl1 = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.row = new DevExpress.XtraVerticalGrid.Rows.PGridEditorRow();
            this.row1 = new DevExpress.XtraVerticalGrid.Rows.PGridEditorRow();
            this.row2 = new DevExpress.XtraVerticalGrid.Rows.PGridEditorRow();
            this.row3 = new DevExpress.XtraVerticalGrid.Rows.PGridEditorRow();
            this.pnlExplorer = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.pnlProcessInfor = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel4_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.galleryDropDown3 = new DevExpress.XtraBars.Ribbon.GalleryDropDown(this.components);
            this.xtraTabbedMdiManager1 = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.styleController1 = new DevExpress.XtraEditors.StyleController(this.components);
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItem14 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem15 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem16 = new DevExpress.XtraBars.BarButtonItem();
            this.panelContainer1 = new DevExpress.XtraBars.Docking.DockPanel();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.galleryDropDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.galleryDropDown4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupGalleryEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDisplayFormat1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.galleryDropDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.pnlSample.SuspendLayout();
            this.pnlProperties.SuspendLayout();
            this.dockPanel5_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.propertyGridControl1)).BeginInit();
            this.pnlExplorer.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            this.pnlProcessInfor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.galleryDropDown3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.styleController1)).BeginInit();
            this.panelContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ApplicationButtonDropDownControl = this.applicationMenu1;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3,
            this.barButtonItem5,
            this.barButtonItem4,
            this.btnPan,
            this.btnVisible,
            this.btnSelection,
            this.btnWindow,
            this.btnPoint,
            this.btnPrevious,
            this.btnNext,
            this.btnZoomOut,
            this.btnIn,
            this.barButtonItem6,
            this.barButtonItem7,
            this.barButtonItem8,
            this.barEditItem1,
            this.barEditItem2,
            this.ribbonGalleryBarItem1,
            this.barButtonItem9,
            this.barButtonItem10,
            this.skinRibbonGalleryBarItem1,
            this.barButtonItem11,
            this.barButtonItem12,
            this.barButtonItem13,
            this.barButtonItem14,
            this.barButtonItem15,
            this.barButtonItem16});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 6;
            this.ribbonControl1.MiniToolbars.Add(this.ribbonMiniToolbar1);
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.PageCategories.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageCategory[] {
            this.ribbonPageCategory1});
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpHome,
            this.rpInsert,
            this.rpModeling,
            this.rpConstruction,
            this.rpView,
            this.rpOutput});
            this.ribbonControl1.QuickToolbarItemLinks.Add(this.barButtonItem9);
            this.ribbonControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1,
            this.repositoryItemComboBox2,
            this.repositoryItemPopupGalleryEdit1,
            this.repositoryItemDisplayFormat1});
            this.ribbonControl1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2007;
            this.ribbonControl1.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.True;
            this.ribbonControl1.ShowDisplayOptionsMenuButton = DevExpress.Utils.DefaultBoolean.True;
            this.ribbonControl1.Size = new System.Drawing.Size(1077, 143);
            // 
            // applicationMenu1
            // 
            this.applicationMenu1.ItemLinks.Add(this.barButtonItem12);
            this.applicationMenu1.ItemLinks.Add(this.barButtonItem13);
            this.applicationMenu1.Name = "applicationMenu1";
            this.applicationMenu1.Ribbon = this.ribbonControl1;
            // 
            // barButtonItem12
            // 
            this.barButtonItem12.Caption = "barButtonItem12";
            this.barButtonItem12.Id = 1;
            this.barButtonItem12.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem12.ImageOptions.Image")));
            this.barButtonItem12.Name = "barButtonItem12";
            // 
            // barButtonItem13
            // 
            this.barButtonItem13.Caption = "barButtonItem13";
            this.barButtonItem13.Id = 2;
            this.barButtonItem13.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem13.ImageOptions.Image")));
            this.barButtonItem13.Name = "barButtonItem13";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Paste";
            this.barButtonItem1.Id = 2;
            this.barButtonItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.ImageOptions.Image")));
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Copy";
            this.barButtonItem2.Id = 3;
            this.barButtonItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.ImageOptions.Image")));
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Cut";
            this.barButtonItem3.Id = 4;
            this.barButtonItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.ImageOptions.Image")));
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "Delete";
            this.barButtonItem5.Id = 6;
            this.barButtonItem5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem5.ImageOptions.Image")));
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Select All";
            this.barButtonItem4.Id = 7;
            this.barButtonItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem4.ImageOptions.Image")));
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // btnPan
            // 
            this.btnPan.Caption = "Pan";
            this.btnPan.Id = 8;
            this.btnPan.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnPan.ImageOptions.Image")));
            this.btnPan.Name = "btnPan";
            this.btnPan.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // btnVisible
            // 
            this.btnVisible.ActAsDropDown = true;
            this.btnVisible.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.btnVisible.Caption = "Visible";
            this.btnVisible.DropDownControl = this.popupMenu1;
            this.btnVisible.Id = 9;
            this.btnVisible.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnVisible.ImageOptions.Image")));
            this.btnVisible.Name = "btnVisible";
            this.btnVisible.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // popupMenu1
            // 
            this.popupMenu1.ItemLinks.Add(this.barButtonItem6);
            this.popupMenu1.ItemLinks.Add(this.barButtonItem7);
            this.popupMenu1.Name = "popupMenu1";
            this.popupMenu1.Ribbon = this.ribbonControl1;
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "Model";
            this.barButtonItem6.Id = 18;
            this.barButtonItem6.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem6.ImageOptions.Image")));
            this.barButtonItem6.Name = "barButtonItem6";
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "Visible";
            this.barButtonItem7.Id = 19;
            this.barButtonItem7.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem7.ImageOptions.Image")));
            this.barButtonItem7.Name = "barButtonItem7";
            // 
            // btnSelection
            // 
            this.btnSelection.Caption = "Selection";
            this.btnSelection.Id = 11;
            this.btnSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSelection.ImageOptions.Image")));
            this.btnSelection.Name = "btnSelection";
            this.btnSelection.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // btnWindow
            // 
            this.btnWindow.Caption = "Window";
            this.btnWindow.Id = 12;
            this.btnWindow.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnWindow.ImageOptions.Image")));
            this.btnWindow.Name = "btnWindow";
            this.btnWindow.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // btnPoint
            // 
            this.btnPoint.Caption = "Point";
            this.btnPoint.Id = 13;
            this.btnPoint.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnPoint.ImageOptions.Image")));
            this.btnPoint.Name = "btnPoint";
            this.btnPoint.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // btnPrevious
            // 
            this.btnPrevious.Caption = "Previous";
            this.btnPrevious.Id = 14;
            this.btnPrevious.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.btnPrevious.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnPrevious.ImageOptions.Image")));
            this.btnPrevious.Name = "btnPrevious";
            // 
            // btnNext
            // 
            this.btnNext.Caption = "Next";
            this.btnNext.Id = 15;
            this.btnNext.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnNext.ImageOptions.Image")));
            this.btnNext.Name = "btnNext";
            // 
            // btnZoomOut
            // 
            this.btnZoomOut.Caption = "Out";
            this.btnZoomOut.Id = 16;
            this.btnZoomOut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnZoomOut.ImageOptions.Image")));
            this.btnZoomOut.Name = "btnZoomOut";
            // 
            // btnIn
            // 
            this.btnIn.Caption = "In";
            this.btnIn.Id = 17;
            this.btnIn.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnIn.ImageOptions.Image")));
            this.btnIn.Name = "btnIn";
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Caption = "Insert";
            this.barButtonItem8.Id = 20;
            this.barButtonItem8.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem8.ImageOptions.Image")));
            this.barButtonItem8.Name = "barButtonItem8";
            this.barButtonItem8.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barEditItem1
            // 
            this.barEditItem1.AutoFillWidthInMenu = DevExpress.Utils.DefaultBoolean.False;
            this.barEditItem1.Caption = "Name           ";
            this.barEditItem1.Edit = this.repositoryItemComboBox1;
            this.barEditItem1.EditWidth = 250;
            this.barEditItem1.Id = 21;
            this.barEditItem1.Name = "barEditItem1";
            this.barEditItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barEditItem1_ItemClick);
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // barEditItem2
            // 
            this.barEditItem2.AutoFillWidthInMenu = DevExpress.Utils.DefaultBoolean.False;
            this.barEditItem2.Caption = "Subtask of : ";
            this.barEditItem2.Edit = this.repositoryItemComboBox2;
            this.barEditItem2.EditWidth = 250;
            this.barEditItem2.Id = 22;
            this.barEditItem2.Name = "barEditItem2";
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.AutoHeight = false;
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            // 
            // ribbonGalleryBarItem1
            // 
            this.ribbonGalleryBarItem1.Caption = "InplaceGallery1";
            // 
            // 
            // 
            galleryItemGroup1.Caption = "Group4";
            galleryItem1.Caption = "Item11";
            galleryItem2.Caption = "Item12";
            galleryItem3.Caption = "Item13";
            galleryItem4.Caption = "Item14";
            galleryItem5.Caption = "Item15";
            galleryItemGroup1.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            galleryItem1,
            galleryItem2,
            galleryItem3,
            galleryItem4,
            galleryItem5});
            this.ribbonGalleryBarItem1.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            galleryItemGroup1});
            this.ribbonGalleryBarItem1.Id = 29;
            this.ribbonGalleryBarItem1.Name = "ribbonGalleryBarItem1";
            // 
            // barButtonItem9
            // 
            this.barButtonItem9.ActAsDropDown = true;
            this.barButtonItem9.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barButtonItem9.Caption = "barButtonItem9";
            this.barButtonItem9.DropDownControl = this.galleryDropDown2;
            this.barButtonItem9.Id = 30;
            this.barButtonItem9.Name = "barButtonItem9";
            this.barButtonItem9.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem9.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem9_ItemClick);
            // 
            // galleryDropDown2
            // 
            // 
            // 
            // 
            galleryItemGroup2.Caption = "Group3";
            galleryItem6.Caption = "Item16";
            galleryItem7.Caption = "Item17";
            galleryItem8.Caption = "Item18";
            galleryItem9.Caption = "Item19";
            galleryItem10.Caption = "Item20";
            galleryItem11.Caption = "Item21";
            galleryItemGroup2.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            galleryItem6,
            galleryItem7,
            galleryItem8,
            galleryItem9,
            galleryItem10,
            galleryItem11});
            this.galleryDropDown2.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            galleryItemGroup2});
            this.galleryDropDown2.Name = "galleryDropDown2";
            this.galleryDropDown2.Ribbon = this.ribbonControl1;
            // 
            // barButtonItem10
            // 
            this.barButtonItem10.Caption = "barButtonItem10";
            this.barButtonItem10.Id = 31;
            this.barButtonItem10.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem10.ImageOptions.Image")));
            this.barButtonItem10.Name = "barButtonItem10";
            this.barButtonItem10.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem10_ItemClick);
            // 
            // skinRibbonGalleryBarItem1
            // 
            this.skinRibbonGalleryBarItem1.Caption = "skinRibbonGalleryBarItem1";
            this.skinRibbonGalleryBarItem1.GalleryDropDown = this.galleryDropDown4;
            this.skinRibbonGalleryBarItem1.Id = 4;
            this.skinRibbonGalleryBarItem1.Name = "skinRibbonGalleryBarItem1";
            // 
            // galleryDropDown4
            // 
            this.galleryDropDown4.Name = "galleryDropDown4";
            this.galleryDropDown4.Ribbon = this.ribbonControl1;
            // 
            // barButtonItem11
            // 
            this.barButtonItem11.Caption = "barButtonItem11";
            this.barButtonItem11.Id = 5;
            this.barButtonItem11.Name = "barButtonItem11";
            // 
            // ribbonMiniToolbar1
            // 
            this.ribbonMiniToolbar1.ItemLinks.Add(this.barButtonItem9);
            this.ribbonMiniToolbar1.ItemLinks.Add(this.barButtonItem10);
            this.ribbonMiniToolbar1.ItemLinks.Add(this.barButtonItem11);
            // 
            // ribbonPageCategory1
            // 
            this.ribbonPageCategory1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(102)))), ((int)(((byte)(255)))));
            this.ribbonPageCategory1.Name = "ribbonPageCategory1";
            this.ribbonPageCategory1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonPageCategory1.Text = "Import Task";
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup2});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "Task";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.AllowTextClipping = false;
            this.ribbonPageGroup1.ItemLinks.Add(this.barEditItem1);
            this.ribbonPageGroup1.ItemLinks.Add(this.barEditItem2);
            this.ribbonPageGroup1.ItemsLayout = DevExpress.XtraBars.Ribbon.RibbonPageGroupItemsLayout.TwoRows;
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Tasks";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.ribbonGalleryBarItem1);
            this.ribbonPageGroup2.ItemLinks.Add(this.skinRibbonGalleryBarItem1);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "Conversation";
            // 
            // rpHome
            // 
            this.rpHome.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgEdit,
            this.rpgNavigation,
            this.rpgMeasureTools,
            this.rpgTasks});
            this.rpHome.Name = "rpHome";
            this.rpHome.Text = "Home";
            // 
            // rpgEdit
            // 
            this.rpgEdit.ItemLinks.Add(this.barButtonItem1);
            this.rpgEdit.ItemLinks.Add(this.barButtonItem2);
            this.rpgEdit.ItemLinks.Add(this.barButtonItem3);
            this.rpgEdit.ItemLinks.Add(this.barButtonItem5);
            this.rpgEdit.ItemLinks.Add(this.barButtonItem4);
            this.rpgEdit.ItemsLayout = DevExpress.XtraBars.Ribbon.RibbonPageGroupItemsLayout.TwoRows;
            this.rpgEdit.Name = "rpgEdit";
            this.rpgEdit.Text = "Edit";
            // 
            // rpgNavigation
            // 
            this.rpgNavigation.ItemLinks.Add(this.btnPan);
            this.rpgNavigation.ItemLinks.Add(this.btnVisible);
            this.rpgNavigation.ItemLinks.Add(this.btnSelection);
            this.rpgNavigation.ItemLinks.Add(this.btnWindow);
            this.rpgNavigation.ItemLinks.Add(this.btnPoint);
            this.rpgNavigation.ItemLinks.Add(this.btnPrevious);
            this.rpgNavigation.ItemLinks.Add(this.btnNext);
            this.rpgNavigation.ItemLinks.Add(this.btnZoomOut);
            this.rpgNavigation.ItemLinks.Add(this.btnIn);
            this.rpgNavigation.ItemsLayout = DevExpress.XtraBars.Ribbon.RibbonPageGroupItemsLayout.TwoRows;
            this.rpgNavigation.Name = "rpgNavigation";
            this.rpgNavigation.Text = "Navigation";
            // 
            // rpgMeasureTools
            // 
            this.rpgMeasureTools.ItemLinks.Add(this.barButtonItem8);
            this.rpgMeasureTools.Name = "rpgMeasureTools";
            this.rpgMeasureTools.Text = "Measure Tools";
            // 
            // rpgTasks
            // 
            this.rpgTasks.Name = "rpgTasks";
            this.rpgTasks.Text = "Tasks";
            // 
            // rpInsert
            // 
            this.rpInsert.Name = "rpInsert";
            this.rpInsert.Text = "Insert";
            // 
            // rpModeling
            // 
            this.rpModeling.Name = "rpModeling";
            this.rpModeling.Text = "Modeling";
            // 
            // rpConstruction
            // 
            this.rpConstruction.Name = "rpConstruction";
            this.rpConstruction.Text = "Construction";
            // 
            // rpView
            // 
            this.rpView.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup3});
            this.rpView.Name = "rpView";
            this.rpView.Text = "View";
            // 
            // rpOutput
            // 
            this.rpOutput.Name = "rpOutput";
            this.rpOutput.Text = "Output";
            // 
            // repositoryItemPopupGalleryEdit1
            // 
            this.repositoryItemPopupGalleryEdit1.AutoHeight = false;
            this.repositoryItemPopupGalleryEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupGalleryEdit1.Name = "repositoryItemPopupGalleryEdit1";
            // 
            // repositoryItemDisplayFormat1
            // 
            this.repositoryItemDisplayFormat1.AutoHeight = false;
            this.repositoryItemDisplayFormat1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDisplayFormat1.Name = "repositoryItemDisplayFormat1";
            // 
            // galleryDropDown1
            // 
            // 
            // 
            // 
            galleryItemGroup3.Caption = "Group1";
            galleryItem12.Caption = "Item1";
            galleryItem13.Caption = "Item2";
            galleryItem14.Caption = "Item3";
            galleryItem15.Caption = "Item4";
            galleryItem16.Caption = "Item5";
            galleryItemGroup3.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            galleryItem12,
            galleryItem13,
            galleryItem14,
            galleryItem15,
            galleryItem16});
            galleryItemGroup4.Caption = "Group2";
            galleryItem17.Caption = "Item6";
            galleryItem18.Caption = "Item7";
            galleryItem19.Caption = "Item8";
            galleryItem20.Caption = "Item9";
            galleryItem21.Caption = "Item10";
            galleryItemGroup4.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            galleryItem17,
            galleryItem18,
            galleryItem19,
            galleryItem20,
            galleryItem21});
            this.galleryDropDown1.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            galleryItemGroup3,
            galleryItemGroup4});
            this.galleryDropDown1.Name = "galleryDropDown1";
            this.galleryDropDown1.Ribbon = this.ribbonControl1;
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.panelContainer1,
            this.pnlProcessInfor,
            this.pnlSample,
            this.pnlSample,
            this.pnlSample});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane",
            "DevExpress.XtraBars.TabFormControl"});
            // 
            // pnlSample
            // 
            this.pnlSample.Controls.Add(this.dockPanel3_Container);
            this.pnlSample.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.pnlSample.FloatVertical = true;
            this.pnlSample.ID = new System.Guid("344236e2-dc10-4a25-89b4-0efe8ddc4954");
            this.pnlSample.Location = new System.Drawing.Point(877, 143);
            this.pnlSample.Name = "pnlSample";
            this.pnlSample.OriginalSize = new System.Drawing.Size(200, 26);
            this.pnlSample.Size = new System.Drawing.Size(200, 146);
            this.pnlSample.Text = "Sample SOSI";
            // 
            // dockPanel3_Container
            // 
            this.dockPanel3_Container.Location = new System.Drawing.Point(5, 23);
            this.dockPanel3_Container.Name = "dockPanel3_Container";
            this.dockPanel3_Container.Size = new System.Drawing.Size(191, 119);
            this.dockPanel3_Container.TabIndex = 0;
            // 
            // pnlProperties
            // 
            this.pnlProperties.Controls.Add(this.dockPanel5_Container);
            this.pnlProperties.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.pnlProperties.ID = new System.Guid("08c7d1cb-f541-4a02-a459-72d71b5234f1");
            this.pnlProperties.Image = ((System.Drawing.Image)(resources.GetObject("pnlProperties.Image")));
            this.pnlProperties.Location = new System.Drawing.Point(0, 0);
            this.pnlProperties.Name = "pnlProperties";
            this.pnlProperties.OriginalSize = new System.Drawing.Size(200, 173);
            this.pnlProperties.Size = new System.Drawing.Size(200, 173);
            this.pnlProperties.Text = "Properties";
            // 
            // dockPanel5_Container
            // 
            this.dockPanel5_Container.Controls.Add(this.propertyGridControl1);
            this.dockPanel5_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel5_Container.Name = "dockPanel5_Container";
            this.dockPanel5_Container.Size = new System.Drawing.Size(191, 145);
            this.dockPanel5_Container.TabIndex = 0;
            // 
            // propertyGridControl1
            // 
            this.propertyGridControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.propertyGridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGridControl1.Location = new System.Drawing.Point(0, 0);
            this.propertyGridControl1.Name = "propertyGridControl1";
            this.propertyGridControl1.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.row,
            this.row1,
            this.row2,
            this.row3});
            this.propertyGridControl1.Size = new System.Drawing.Size(191, 145);
            this.propertyGridControl1.TabIndex = 0;
            // 
            // row
            // 
            this.row.Height = 16;
            this.row.IsChildRowsLoaded = true;
            this.row.Name = "row";
            // 
            // row1
            // 
            this.row1.Expanded = false;
            this.row1.Name = "row1";
            // 
            // row2
            // 
            this.row2.Expanded = false;
            this.row2.Name = "row2";
            // 
            // row3
            // 
            this.row3.Expanded = false;
            this.row3.Name = "row3";
            // 
            // pnlExplorer
            // 
            this.pnlExplorer.Controls.Add(this.dockPanel1_Container);
            this.pnlExplorer.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.pnlExplorer.ID = new System.Guid("2de88d59-ec81-412f-b704-fb282c0937f1");
            this.pnlExplorer.Location = new System.Drawing.Point(0, 173);
            this.pnlExplorer.Name = "pnlExplorer";
            this.pnlExplorer.OriginalSize = new System.Drawing.Size(200, 200);
            this.pnlExplorer.Size = new System.Drawing.Size(200, 173);
            this.pnlExplorer.Text = "Explorer";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.treeView1);
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(191, 146);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // treeView1
            // 
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView1.Location = new System.Drawing.Point(0, 0);
            this.treeView1.Name = "treeView1";
            treeNode1.Name = "Node1";
            treeNode1.Text = "Node1";
            treeNode2.Name = "Node2";
            treeNode2.Text = "Node2";
            treeNode3.Name = "Node3";
            treeNode3.Text = "Node3";
            treeNode4.Name = "Node4";
            treeNode4.Text = "Node4";
            treeNode5.Name = "Node0";
            treeNode5.Text = "Node0";
            treeNode6.Name = "Node6";
            treeNode6.Text = "Node6";
            treeNode7.Name = "Node7";
            treeNode7.Text = "Node7";
            treeNode8.Name = "Node8";
            treeNode8.Text = "Node8";
            treeNode9.Name = "Node9";
            treeNode9.Text = "Node9";
            treeNode10.Name = "Node10";
            treeNode10.Text = "Node10";
            treeNode11.Name = "Node5";
            treeNode11.Text = "Node5";
            this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode5,
            treeNode11});
            this.treeView1.Size = new System.Drawing.Size(191, 146);
            this.treeView1.TabIndex = 0;
            // 
            // pnlProcessInfor
            // 
            this.pnlProcessInfor.Controls.Add(this.dockPanel4_Container);
            this.pnlProcessInfor.Dock = DevExpress.XtraBars.Docking.DockingStyle.Bottom;
            this.pnlProcessInfor.ID = new System.Guid("6e7d99a1-b68c-4e18-aed3-8305a8efdcaa");
            this.pnlProcessInfor.Location = new System.Drawing.Point(200, 289);
            this.pnlProcessInfor.Name = "pnlProcessInfor";
            this.pnlProcessInfor.OriginalSize = new System.Drawing.Size(200, 200);
            this.pnlProcessInfor.Size = new System.Drawing.Size(877, 200);
            this.pnlProcessInfor.Text = "Process Infor";
            // 
            // dockPanel4_Container
            // 
            this.dockPanel4_Container.Location = new System.Drawing.Point(4, 24);
            this.dockPanel4_Container.Name = "dockPanel4_Container";
            this.dockPanel4_Container.Size = new System.Drawing.Size(869, 172);
            this.dockPanel4_Container.TabIndex = 0;
            // 
            // galleryDropDown3
            // 
            this.galleryDropDown3.Name = "galleryDropDown3";
            this.galleryDropDown3.Ribbon = this.ribbonControl1;
            // 
            // xtraTabbedMdiManager1
            // 
            this.xtraTabbedMdiManager1.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InAllTabPageHeaders;
            this.xtraTabbedMdiManager1.FloatOnDrag = DevExpress.Utils.DefaultBoolean.True;
            this.xtraTabbedMdiManager1.FloatPageDragMode = DevExpress.XtraTabbedMdi.FloatPageDragMode.Preview;
            this.xtraTabbedMdiManager1.HeaderButtons = DevExpress.XtraTab.TabButtons.Close;
            this.xtraTabbedMdiManager1.MdiParent = this;
            // 
            // styleController1
            // 
            this.styleController1.LookAndFeel.SkinName = "Seven Classic";
            this.styleController1.LookAndFeel.UseDefaultLookAndFeel = false;
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem14);
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem15);
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem16);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "ribbonPageGroup3";
            // 
            // barButtonItem14
            // 
            this.barButtonItem14.Caption = "View Explorer";
            this.barButtonItem14.Id = 3;
            this.barButtonItem14.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem14.ImageOptions.Image")));
            this.barButtonItem14.Name = "barButtonItem14";
            this.barButtonItem14.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem14_ItemClick);
            // 
            // barButtonItem15
            // 
            this.barButtonItem15.Caption = "View Properties";
            this.barButtonItem15.Id = 4;
            this.barButtonItem15.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem15.ImageOptions.Image")));
            this.barButtonItem15.Name = "barButtonItem15";
            // 
            // barButtonItem16
            // 
            this.barButtonItem16.Caption = "View Toolbar";
            this.barButtonItem16.Id = 5;
            this.barButtonItem16.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem16.ImageOptions.Image")));
            this.barButtonItem16.Name = "barButtonItem16";
            // 
            // panelContainer1
            // 
            this.panelContainer1.Controls.Add(this.pnlProperties);
            this.panelContainer1.Controls.Add(this.pnlExplorer);
            this.panelContainer1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.panelContainer1.ID = new System.Guid("3ff44252-8ad1-4a8e-94db-dffad74aec92");
            this.panelContainer1.Location = new System.Drawing.Point(0, 143);
            this.panelContainer1.Name = "panelContainer1";
            this.panelContainer1.OriginalSize = new System.Drawing.Size(200, 200);
            this.panelContainer1.Size = new System.Drawing.Size(200, 346);
            this.panelContainer1.Text = "panelContainer1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1077, 489);
            this.Controls.Add(this.pnlSample);
            this.Controls.Add(this.pnlProcessInfor);
            this.Controls.Add(this.panelContainer1);
            this.Controls.Add(this.ribbonControl1);
            this.IsMdiContainer = true;
            this.Name = "Form1";
            this.Ribbon = this.ribbonControl1;
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.galleryDropDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.galleryDropDown4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupGalleryEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDisplayFormat1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.galleryDropDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.pnlSample.ResumeLayout(false);
            this.pnlProperties.ResumeLayout(false);
            this.dockPanel5_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.propertyGridControl1)).EndInit();
            this.pnlExplorer.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            this.pnlProcessInfor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.galleryDropDown3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.styleController1)).EndInit();
            this.panelContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpHome;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgEdit;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpInsert;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpModeling;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpConstruction;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpView;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpOutput;
        private DevExpress.XtraBars.Ribbon.GalleryDropDown galleryDropDown1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgNavigation;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgMeasureTools;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgTasks;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel pnlProperties;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel5_Container;
        private DevExpress.XtraBars.Docking.DockPanel pnlProcessInfor;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel4_Container;
        private DevExpress.XtraBars.Docking.DockPanel pnlExplorer;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraBars.Docking.DockPanel pnlSample;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel3_Container;
        private DevExpress.XtraBars.BarButtonItem btnPan;
        private DevExpress.XtraBars.BarButtonItem btnVisible;
        private DevExpress.XtraBars.BarButtonItem btnSelection;
        private DevExpress.XtraBars.BarButtonItem btnWindow;
        private DevExpress.XtraBars.BarButtonItem btnPoint;
        private DevExpress.XtraBars.BarButtonItem btnPrevious;
        private DevExpress.XtraBars.BarButtonItem btnNext;
        private DevExpress.XtraBars.BarButtonItem btnZoomOut;
        private DevExpress.XtraBars.BarButtonItem btnIn;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.BarButtonItem barButtonItem8;
        private DevExpress.XtraBars.Ribbon.RibbonPageCategory ribbonPageCategory1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraBars.BarEditItem barEditItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupGalleryEdit repositoryItemPopupGalleryEdit1;
        private DevExpress.XtraRichEdit.Forms.Design.RepositoryItemDisplayFormat repositoryItemDisplayFormat1;
        private DevExpress.XtraBars.RibbonGalleryBarItem ribbonGalleryBarItem1;
        private DevExpress.XtraBars.Ribbon.GalleryDropDown galleryDropDown2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem9;
        private DevExpress.XtraBars.Ribbon.GalleryDropDown galleryDropDown3;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager1;
        private System.Windows.Forms.TreeView treeView1;
        private DevExpress.XtraVerticalGrid.PropertyGridControl propertyGridControl1;
        private DevExpress.XtraVerticalGrid.Rows.PGridEditorRow row;
        private DevExpress.XtraVerticalGrid.Rows.PGridEditorRow row1;
        private DevExpress.XtraVerticalGrid.Rows.PGridEditorRow row2;
        private DevExpress.XtraVerticalGrid.Rows.PGridEditorRow row3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem10;
        private DevExpress.XtraBars.Ribbon.RibbonMiniToolbar ribbonMiniToolbar1;
        private DevExpress.XtraEditors.StyleController styleController1;
        private DevExpress.XtraBars.SkinRibbonGalleryBarItem skinRibbonGalleryBarItem1;
        private DevExpress.XtraBars.Ribbon.GalleryDropDown galleryDropDown4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem11;
        private DevExpress.XtraBars.Ribbon.ApplicationMenu applicationMenu1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem12;
        private DevExpress.XtraBars.BarButtonItem barButtonItem13;
        private DevExpress.XtraBars.BarButtonItem barButtonItem14;
        private DevExpress.XtraBars.BarButtonItem barButtonItem15;
        private DevExpress.XtraBars.BarButtonItem barButtonItem16;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.Docking.DockPanel panelContainer1;
    }
}

