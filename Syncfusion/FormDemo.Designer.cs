﻿namespace WindowsFormsApplication626
{
    partial class FormDemo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Syncfusion.Windows.Forms.Tools.ToolStripTabGroup toolStripTabGroup1 = new Syncfusion.Windows.Forms.Tools.ToolStripTabGroup();
            Syncfusion.Windows.Forms.Tools.ToolStripGalleryItem toolStripGalleryItem1 = new Syncfusion.Windows.Forms.Tools.ToolStripGalleryItem();
            Syncfusion.Windows.Forms.Tools.ToolStripGalleryItem toolStripGalleryItem2 = new Syncfusion.Windows.Forms.Tools.ToolStripGalleryItem();
            Syncfusion.Windows.Forms.Tools.ToolStripGalleryItem toolStripGalleryItem3 = new Syncfusion.Windows.Forms.Tools.ToolStripGalleryItem();
            Syncfusion.Windows.Forms.Tools.ToolStripGalleryItem toolStripGalleryItem4 = new Syncfusion.Windows.Forms.Tools.ToolStripGalleryItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDemo));
            this.ribbonControlAdv1 = new Syncfusion.Windows.Forms.Tools.RibbonControlAdv();
            this.backStageView1 = new Syncfusion.Windows.Forms.BackStageView(this.components);
            this.Print = new Syncfusion.Windows.Forms.BackStage();
            this.backStageButton1 = new Syncfusion.Windows.Forms.BackStageButton();
            this.backStageButton3 = new Syncfusion.Windows.Forms.BackStageButton();
            this.backStageTab1 = new Syncfusion.Windows.Forms.BackStageTab();
            this.toolStripTabItem1 = new Syncfusion.Windows.Forms.Tools.ToolStripTabItem();
            this.toolStripEx1 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripPanelItem1 = new Syncfusion.Windows.Forms.Tools.ToolStripPanelItem();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripEx2 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.toolStripPanelItem3 = new Syncfusion.Windows.Forms.Tools.ToolStripPanelItem();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripPanelItem2 = new Syncfusion.Windows.Forms.Tools.ToolStripPanelItem();
            this.name = new Syncfusion.Windows.Forms.Tools.ToolStripComboBoxEx();
            this.Subtask = new Syncfusion.Windows.Forms.Tools.ToolStripComboBoxEx();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.saveASToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripEx3 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.toolStripGallery1 = new Syncfusion.Windows.Forms.Tools.ToolStripGallery();
            this.toolStripTabItem2 = new Syncfusion.Windows.Forms.Tools.ToolStripTabItem();
            this.View = new Syncfusion.Windows.Forms.Tools.ToolStripTabItem();
            this.toolStripEx4 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.dockingClientPanel1 = new Syncfusion.Windows.Forms.Tools.DockingClientPanel();
            this.Output = new System.Windows.Forms.Panel();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.Properties = new System.Windows.Forms.Panel();
            this.New = new System.Windows.Forms.Panel();
            this.dockingManager1 = new Syncfusion.Windows.Forms.Tools.DockingManager(this.components);
            this.backStageButton2 = new Syncfusion.Windows.Forms.BackStageButton();
            this.miniToolBar1 = new Syncfusion.Windows.Forms.Tools.MiniToolBar();
            this.Cut = new System.Windows.Forms.ToolStripButton();
            this.Copy = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControlAdv1)).BeginInit();
            this.ribbonControlAdv1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Print)).BeginInit();
            this.Print.SuspendLayout();
            this.toolStripTabItem1.Panel.SuspendLayout();
            this.toolStripEx1.SuspendLayout();
            this.toolStripEx2.SuspendLayout();
            this.toolStripEx3.SuspendLayout();
            this.View.Panel.SuspendLayout();
            this.toolStripEx4.SuspendLayout();
            this.dockingClientPanel1.SuspendLayout();
            this.Output.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dockingManager1)).BeginInit();
            this.miniToolBar1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ribbonControlAdv1
            // 
            this.ribbonControlAdv1.BackStageView = this.backStageView1;
            this.ribbonControlAdv1.CaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ribbonControlAdv1.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.ribbonControlAdv1.Header.AddMainItem(toolStripTabItem1);
            this.ribbonControlAdv1.Header.AddMainItem(toolStripTabItem2);
            this.ribbonControlAdv1.Header.AddMainItem(View);
            this.ribbonControlAdv1.Header.AddQuickItem(new Syncfusion.Windows.Forms.Tools.QuickButtonReflectable(toolStripButton1));
            this.ribbonControlAdv1.Header.AddQuickItem(new Syncfusion.Windows.Forms.Tools.QuickButtonReflectable(toolStripButton4));
            this.ribbonControlAdv1.Header.AddQuickItem(new Syncfusion.Windows.Forms.Tools.QuickButtonReflectable(toolStripButton3));
            this.ribbonControlAdv1.Location = new System.Drawing.Point(1, 0);
            this.ribbonControlAdv1.MenuButtonFont = new System.Drawing.Font("Segoe UI", 8.25F);
            this.ribbonControlAdv1.MenuButtonText = "File";
            this.ribbonControlAdv1.MenuColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(114)))), ((int)(((byte)(198)))));
            this.ribbonControlAdv1.Name = "ribbonControlAdv1";
            this.ribbonControlAdv1.OfficeColorScheme = Syncfusion.Windows.Forms.Tools.ToolStripEx.ColorScheme.Managed;
            // 
            // ribbonControlAdv1.OfficeMenu
            // 
            this.ribbonControlAdv1.OfficeMenu.BackColor = System.Drawing.Color.Maroon;
            this.ribbonControlAdv1.OfficeMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ribbonControlAdv1.OfficeMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.ribbonControlAdv1.OfficeMenu.Name = "OfficeMenu";
            this.ribbonControlAdv1.OfficeMenu.ShowItemToolTips = true;
            this.ribbonControlAdv1.OfficeMenu.Size = new System.Drawing.Size(12, 65);
            this.ribbonControlAdv1.QuickPanelImageLayout = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ribbonControlAdv1.RibbonHeaderImage = Syncfusion.Windows.Forms.Tools.RibbonHeaderImage.None;
            this.ribbonControlAdv1.SelectedTab = this.toolStripTabItem1;
            this.ribbonControlAdv1.ShowRibbonDisplayOptionButton = true;
            this.ribbonControlAdv1.Size = new System.Drawing.Size(1150, 164);
            this.ribbonControlAdv1.SystemText.QuickAccessDialogDropDownName = "Start menu";
            this.ribbonControlAdv1.SystemText.RenameDisplayLabelText = "&Display Name:";
            toolStripTabGroup1.Color = System.Drawing.Color.Empty;
            toolStripTabGroup1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            toolStripTabGroup1.Name = "Format";
            toolStripTabGroup1.Visible = true;
            this.ribbonControlAdv1.TabGroups.Add(toolStripTabGroup1);
            this.ribbonControlAdv1.TabGroups.SetTabGroup(View, toolStripTabGroup1);
            this.ribbonControlAdv1.TabIndex = 0;
            this.ribbonControlAdv1.Text = "ribbonControlAdv1";
            this.ribbonControlAdv1.TitleColor = System.Drawing.Color.Black;
            // 
            // backStageView1
            // 
            this.backStageView1.BackStage = this.Print;
            this.backStageView1.HostControl = null;
            this.backStageView1.HostForm = this;
            // 
            // Print
            // 
            this.Print.ActiveTabForeColor = System.Drawing.Color.Empty;
            this.Print.AllowDrop = true;
            this.Print.BeforeTouchSize = new System.Drawing.Size(1137, 713);
            this.Print.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Print.ChildItemSize = new System.Drawing.Size(80, 140);
            this.Print.CloseButtonForeColor = System.Drawing.Color.Empty;
            this.Print.CloseButtonHoverForeColor = System.Drawing.Color.Empty;
            this.Print.CloseButtonPressedForeColor = System.Drawing.Color.Empty;
            this.Print.Controls.Add(this.backStageButton1);
            this.Print.Controls.Add(this.backStageButton3);
            this.Print.Controls.Add(this.backStageTab1);
            this.Print.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.Print.InActiveTabForeColor = System.Drawing.Color.Empty;
            this.Print.ItemSize = new System.Drawing.Size(130, 40);
            this.Print.Location = new System.Drawing.Point(0, 0);
            this.Print.Name = "backStage1";
            this.Print.OfficeColorScheme = Syncfusion.Windows.Forms.Tools.ToolStripEx.ColorScheme.Managed;
            this.Print.SeparatorColor = System.Drawing.SystemColors.ControlDark;
            this.Print.ShowSeparator = false;
            this.Print.Size = new System.Drawing.Size(1137, 713);
            this.Print.TabIndex = 2;
            this.Print.Visible = false;
            // 
            // backStageButton1
            // 
            this.backStageButton1.Accelerator = "";
            this.backStageButton1.BackColor = System.Drawing.Color.Transparent;
            this.backStageButton1.BeforeTouchSize = new System.Drawing.Size(75, 23);
            this.backStageButton1.Image = global::WindowsFormsApplication626.Properties.Resources.SaveAs32;
            this.backStageButton1.IsBackStageButton = false;
            this.backStageButton1.Location = new System.Drawing.Point(10, 10);
            this.backStageButton1.Name = "backStageButton1";
            this.backStageButton1.Size = new System.Drawing.Size(110, 25);
            this.backStageButton1.TabIndex = 2;
            this.backStageButton1.Text = "Save As";
            // 
            // backStageButton3
            // 
            this.backStageButton3.Accelerator = "";
            this.backStageButton3.BackColor = System.Drawing.Color.Transparent;
            this.backStageButton3.BeforeTouchSize = new System.Drawing.Size(75, 23);
            this.backStageButton3.Image = global::WindowsFormsApplication626.Properties.Resources.Exit;
            this.backStageButton3.IsBackStageButton = false;
            this.backStageButton3.Location = new System.Drawing.Point(10, 35);
            this.backStageButton3.Name = "backStageButton3";
            this.backStageButton3.Size = new System.Drawing.Size(110, 25);
            this.backStageButton3.TabIndex = 3;
            this.backStageButton3.Text = "Exit";
            // 
            // backStageTab1
            // 
            this.backStageTab1.Accelerator = "";
            this.backStageTab1.BackColor = System.Drawing.Color.White;
            this.backStageTab1.Image = null;
            this.backStageTab1.ImageSize = new System.Drawing.Size(16, 16);
            this.backStageTab1.Location = new System.Drawing.Point(129, 0);
            this.backStageTab1.Name = "backStageTab1";
            this.backStageTab1.Position = new System.Drawing.Point(11, 55);
            this.backStageTab1.ShowCloseButton = true;
            this.backStageTab1.Size = new System.Drawing.Size(1008, 713);
            this.backStageTab1.TabIndex = 4;
            this.backStageTab1.Text = "Print";
            this.backStageTab1.ThemesEnabled = false;
            // 
            // toolStripTabItem1
            // 
            this.toolStripTabItem1.Name = "toolStripTabItem1";
            // 
            // ribbonControlAdv1.ribbonPanel1
            // 
            this.toolStripTabItem1.Panel.Controls.Add(this.toolStripEx1);
            this.toolStripTabItem1.Panel.Controls.Add(this.toolStripEx2);
            this.toolStripTabItem1.Panel.Controls.Add(this.toolStripEx3);
            this.toolStripTabItem1.Panel.Name = "ribbonPanel1";
            this.toolStripTabItem1.Panel.ScrollPosition = 0;
            this.toolStripTabItem1.Panel.TabIndex = 2;
            this.toolStripTabItem1.Panel.Text = "Home";
            this.toolStripTabItem1.Position = 0;
            this.toolStripTabItem1.Size = new System.Drawing.Size(52, 25);
            this.toolStripTabItem1.Tag = "1";
            this.toolStripTabItem1.Text = "Home";
            // 
            // toolStripEx1
            // 
            this.toolStripEx1.AutoSize = false;
            this.toolStripEx1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx1.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.toolStripEx1.ForeColor = System.Drawing.Color.MidnightBlue;
            this.toolStripEx1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx1.Image = null;
            this.toolStripEx1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStripEx1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton4,
            this.toolStripButton3,
            this.toolStripPanelItem1});
            this.toolStripEx1.Location = new System.Drawing.Point(0, 1);
            this.toolStripEx1.Name = "toolStripEx1";
            this.toolStripEx1.Size = new System.Drawing.Size(373, 98);
            this.toolStripEx1.TabIndex = 0;
            this.toolStripEx1.Text = "Button Demo";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::WindowsFormsApplication626.Properties.Resources.BlankPage32;
            this.toolStripButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(40, 74);
            this.toolStripButton1.Text = "New";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.Image = global::WindowsFormsApplication626.Properties.Resources.Paste32;
            this.toolStripButton4.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(46, 74);
            this.toolStripButton4.Text = "Paste";
            this.toolStripButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(85, 74);
            this.toolStripButton3.Text = "Attachment";
            this.toolStripButton3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripPanelItem1
            // 
            this.toolStripPanelItem1.CausesValidation = false;
            this.toolStripPanelItem1.ForeColor = System.Drawing.Color.MidnightBlue;
            this.toolStripPanelItem1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton2,
            this.toolStripButton7,
            this.toolStripButton8});
            this.toolStripPanelItem1.Name = "toolStripPanelItem1";
            this.toolStripPanelItem1.RowCount = 2;
            this.toolStripPanelItem1.Size = new System.Drawing.Size(148, 77);
            this.toolStripPanelItem1.Text = "toolStripPanelItem1";
            this.toolStripPanelItem1.Transparent = true;
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = global::WindowsFormsApplication626.Properties.Resources.Copy16;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(67, 24);
            this.toolStripButton2.Text = "Copy";
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.Image = global::WindowsFormsApplication626.Properties.Resources.Cut16;
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(55, 24);
            this.toolStripButton7.Text = "Cut";
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.Image = global::WindowsFormsApplication626.Properties.Resources.Exit;
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.Size = new System.Drawing.Size(77, 24);
            this.toolStripButton8.Text = "Delete";
            // 
            // toolStripEx2
            // 
            this.toolStripEx2.AutoSize = false;
            this.toolStripEx2.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx2.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.toolStripEx2.ForeColor = System.Drawing.Color.MidnightBlue;
            this.toolStripEx2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx2.Image = null;
            this.toolStripEx2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStripEx2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripPanelItem3,
            this.toolStripPanelItem2,
            this.toolStripDropDownButton1});
            this.toolStripEx2.Location = new System.Drawing.Point(375, 1);
            this.toolStripEx2.Name = "toolStripEx2";
            this.toolStripEx2.Size = new System.Drawing.Size(323, 98);
            this.toolStripEx2.TabIndex = 1;
            this.toolStripEx2.Text = "DropDown Demo";
            // 
            // toolStripPanelItem3
            // 
            this.toolStripPanelItem3.CausesValidation = false;
            this.toolStripPanelItem3.ForeColor = System.Drawing.Color.MidnightBlue;
            this.toolStripPanelItem3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel2,
            this.toolStripLabel3});
            this.toolStripPanelItem3.Name = "toolStripPanelItem3";
            this.toolStripPanelItem3.Size = new System.Drawing.Size(95, 77);
            this.toolStripPanelItem3.Text = "toolStripPanelItem3";
            this.toolStripPanelItem3.Transparent = true;
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Margin = new System.Windows.Forms.Padding(5);
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(52, 20);
            this.toolStripLabel2.Text = "Name:";
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Margin = new System.Windows.Forms.Padding(3, 13, 3, 3);
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(85, 20);
            this.toolStripLabel3.Text = "Subtask of: ";
            // 
            // toolStripPanelItem2
            // 
            this.toolStripPanelItem2.CausesValidation = false;
            this.toolStripPanelItem2.ForeColor = System.Drawing.Color.MidnightBlue;
            this.toolStripPanelItem2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.name,
            this.Subtask});
            this.toolStripPanelItem2.Margin = new System.Windows.Forms.Padding(0);
            this.toolStripPanelItem2.Name = "toolStripPanelItem2";
            this.toolStripPanelItem2.Size = new System.Drawing.Size(135, 77);
            this.toolStripPanelItem2.Text = "toolStripPanelItem2";
            this.toolStripPanelItem2.Transparent = true;
            // 
            // name
            // 
            this.name.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.name.Items.AddRange(new object[] {
            "Item 1",
            "Item 2 ",
            "Item 3",
            "Item 1",
            "Item 2 ",
            "Item 3"});
            this.name.Margin = new System.Windows.Forms.Padding(5);
            this.name.MaxLength = 32767;
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(121, 28);
            this.name.Tag = "Name: ";
            this.ribbonControlAdv1.SetUseInCustomQuickAccessDialog(this.name, false);
            // 
            // Subtask
            // 
            this.Subtask.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.Subtask.Items.AddRange(new object[] {
            "/51 ROAD DRAINAGE",
            "/10 IMPORT",
            "/10 IMPORT/101 FROM SOSI",
            "/51 ROAD DRAINAGE",
            "/10 IMPORT",
            "/10 IMPORT/101 FROM SOSI"});
            this.Subtask.Margin = new System.Windows.Forms.Padding(5, 2, 5, 5);
            this.Subtask.MaxLength = 32767;
            this.Subtask.Name = "Subtask";
            this.Subtask.Size = new System.Drawing.Size(121, 28);
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveASToolStripMenuItem,
            this.saveToolStripMenuItem});
            this.toolStripDropDownButton1.Image = global::WindowsFormsApplication626.Properties.Resources.Save32;
            this.toolStripDropDownButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(51, 74);
            this.toolStripDropDownButton1.Text = "Save";
            this.toolStripDropDownButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // saveASToolStripMenuItem
            // 
            this.saveASToolStripMenuItem.Image = global::WindowsFormsApplication626.Properties.Resources.SaveAs32;
            this.saveASToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.saveASToolStripMenuItem.Name = "saveASToolStripMenuItem";
            this.saveASToolStripMenuItem.Size = new System.Drawing.Size(143, 38);
            this.saveASToolStripMenuItem.Text = "Save As";
            this.saveASToolStripMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Image = global::WindowsFormsApplication626.Properties.Resources.Save32;
            this.saveToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(143, 38);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripEx3
            // 
            this.toolStripEx3.AutoSize = false;
            this.toolStripEx3.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx3.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.toolStripEx3.ForeColor = System.Drawing.Color.MidnightBlue;
            this.toolStripEx3.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx3.Image = null;
            this.toolStripEx3.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStripEx3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripGallery1});
            this.toolStripEx3.Location = new System.Drawing.Point(700, 1);
            this.toolStripEx3.Name = "toolStripEx3";
            this.toolStripEx3.Size = new System.Drawing.Size(319, 98);
            this.toolStripEx3.TabIndex = 2;
            this.toolStripEx3.Text = "DropDown Button Demo";
            // 
            // toolStripGallery1
            // 
            this.toolStripGallery1.CaptionText = "";
            this.toolStripGallery1.CheckOnClick = true;
            this.toolStripGallery1.ItemBackColor = System.Drawing.Color.Empty;
            toolStripGalleryItem1.Image = global::WindowsFormsApplication626.Properties.Resources._0202_InsertShape_32;
            toolStripGalleryItem1.Text = "Shapes";
            toolStripGalleryItem1.ToolTipText = "";
            toolStripGalleryItem2.Image = global::WindowsFormsApplication626.Properties.Resources.align_left;
            toolStripGalleryItem2.Text = "Align left";
            toolStripGalleryItem2.ToolTipText = "";
            toolStripGalleryItem3.Image = global::WindowsFormsApplication626.Properties.Resources.base_business_contacts;
            toolStripGalleryItem3.Text = "Contact";
            toolStripGalleryItem3.ToolTipText = "";
            toolStripGalleryItem4.Image = global::WindowsFormsApplication626.Properties.Resources.base_charts;
            toolStripGalleryItem4.Text = "Chart";
            toolStripGalleryItem4.ToolTipText = "";
            this.toolStripGallery1.Items.Add(toolStripGalleryItem1);
            this.toolStripGallery1.Items.Add(toolStripGalleryItem2);
            this.toolStripGallery1.Items.Add(toolStripGalleryItem3);
            this.toolStripGallery1.Items.Add(toolStripGalleryItem4);
            this.toolStripGallery1.Name = "toolStripGallery1";
            this.toolStripGallery1.ScrollerType = Syncfusion.Windows.Forms.Tools.ToolStripGalleryScrollerType.Standard;
            this.toolStripGallery1.ShowToolTip = true;
            this.toolStripGallery1.Size = new System.Drawing.Size(231, 47);
            this.toolStripGallery1.Text = "toolStripGallery1";
            // 
            // toolStripTabItem2
            // 
            this.toolStripTabItem2.Name = "toolStripTabItem2";
            // 
            // ribbonControlAdv1.ribbonPanel2
            // 
            this.toolStripTabItem2.Panel.Name = "ribbonPanel2";
            this.toolStripTabItem2.Panel.ScrollPosition = 0;
            this.toolStripTabItem2.Panel.TabIndex = 3;
            this.toolStripTabItem2.Panel.Text = "Modelling";
            this.toolStripTabItem2.Position = 1;
            this.toolStripTabItem2.Size = new System.Drawing.Size(76, 25);
            this.toolStripTabItem2.Tag = "2";
            this.toolStripTabItem2.Text = "Modelling";
            // 
            // View
            // 
            this.View.Name = "View";
            // 
            // ribbonControlAdv1.ribbonPanel3
            // 
            this.View.Panel.Controls.Add(this.toolStripEx4);
            this.View.Panel.Name = "ribbonPanel3";
            this.View.Panel.ScrollPosition = 0;
            this.View.Panel.TabIndex = 4;
            this.View.Panel.Text = "View";
            this.View.Position = 2;
            this.View.Size = new System.Drawing.Size(44, 25);
            this.View.Tag = "3";
            this.View.Text = "View";
            // 
            // toolStripEx4
            // 
            this.toolStripEx4.AutoSize = false;
            this.toolStripEx4.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx4.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.toolStripEx4.ForeColor = System.Drawing.Color.MidnightBlue;
            this.toolStripEx4.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx4.Image = null;
            this.toolStripEx4.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStripEx4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton5});
            this.toolStripEx4.Location = new System.Drawing.Point(0, 1);
            this.toolStripEx4.Name = "toolStripEx4";
            this.toolStripEx4.Size = new System.Drawing.Size(141, 98);
            this.toolStripEx4.TabIndex = 0;
            this.toolStripEx4.Text = "Edit";
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.Image = global::WindowsFormsApplication626.Properties.Resources.CoverPage32;
            this.toolStripButton5.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(45, 74);
            this.toolStripButton5.Text = "Copy";
            this.toolStripButton5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // dockingClientPanel1
            // 
            this.dockingClientPanel1.Controls.Add(this.Output);
            this.dockingClientPanel1.Controls.Add(this.Properties);
            this.dockingClientPanel1.Location = new System.Drawing.Point(6, 165);
            this.dockingClientPanel1.Name = "dockingClientPanel1";
            this.dockingClientPanel1.Size = new System.Drawing.Size(1140, 623);
            this.dockingClientPanel1.SizeToFit = true;
            this.dockingClientPanel1.TabIndex = 1;
            // 
            // Output
            // 
            this.Output.Controls.Add(this.listBox1);
            this.Output.Location = new System.Drawing.Point(3, 437);
            this.Output.Name = "Output";
            this.Output.Size = new System.Drawing.Size(1134, 174);
            this.Output.TabIndex = 0;
            // 
            // listBox1
            // 
            this.listBox1.BackColor = System.Drawing.SystemColors.Control;
            this.listBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Items.AddRange(new object[] {
            "test 1",
            "test 2",
            "test 3",
            "test 4",
            "test 5",
            "test 6"});
            this.listBox1.Location = new System.Drawing.Point(40, 20);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(407, 128);
            this.listBox1.TabIndex = 0;
            this.listBox1.Click += new System.EventHandler(this.listBox1_Click);
            // 
            // Properties
            // 
            this.Properties.Location = new System.Drawing.Point(901, 0);
            this.Properties.Name = "Properties";
            this.Properties.Size = new System.Drawing.Size(236, 415);
            this.Properties.TabIndex = 2;
            // 
            // New
            // 
            this.New.Location = new System.Drawing.Point(10, 164);
            this.New.Name = "New";
            this.New.Size = new System.Drawing.Size(537, 360);
            this.New.TabIndex = 1;
            // 
            // dockingManager1
            // 
            this.dockingManager1.ActiveCaptionFont = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World);
            this.dockingManager1.AnimateAutoHiddenWindow = true;
            this.dockingManager1.AutoHideTabForeColor = System.Drawing.Color.Empty;
            this.dockingManager1.DockedCaptionFont = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World);
            this.dockingManager1.DockLayoutStream = ((System.IO.MemoryStream)(resources.GetObject("dockingManager1.DockLayoutStream")));
            this.dockingManager1.DockTabForeColor = System.Drawing.Color.Empty;
            this.dockingManager1.HostControl = this;
            this.dockingManager1.InActiveCaptionFont = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World);
            this.dockingManager1.MetroButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dockingManager1.MetroColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(158)))), ((int)(((byte)(218)))));
            this.dockingManager1.MetroSplitterBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(159)))), ((int)(((byte)(183)))));
            this.dockingManager1.ReduceFlickeringInRtl = false;
            this.dockingManager1.CaptionButtons.Add(new Syncfusion.Windows.Forms.Tools.CaptionButton(Syncfusion.Windows.Forms.Tools.CaptionButtonType.Close, "CloseButton"));
            this.dockingManager1.CaptionButtons.Add(new Syncfusion.Windows.Forms.Tools.CaptionButton(Syncfusion.Windows.Forms.Tools.CaptionButtonType.Pin, "PinButton"));
            this.dockingManager1.CaptionButtons.Add(new Syncfusion.Windows.Forms.Tools.CaptionButton(Syncfusion.Windows.Forms.Tools.CaptionButtonType.Maximize, "MaximizeButton"));
            this.dockingManager1.CaptionButtons.Add(new Syncfusion.Windows.Forms.Tools.CaptionButton(Syncfusion.Windows.Forms.Tools.CaptionButtonType.Restore, "RestoreButton"));
            this.dockingManager1.CaptionButtons.Add(new Syncfusion.Windows.Forms.Tools.CaptionButton(Syncfusion.Windows.Forms.Tools.CaptionButtonType.Menu, "MenuButton"));
            // 
            // backStageButton2
            // 
            this.backStageButton2.Accelerator = "";
            this.backStageButton2.BackColor = System.Drawing.Color.Transparent;
            this.backStageButton2.BeforeTouchSize = new System.Drawing.Size(75, 23);
            this.backStageButton2.Image = global::WindowsFormsApplication626.Properties.Resources.Exit;
            this.backStageButton2.IsBackStageButton = false;
            this.backStageButton2.Location = new System.Drawing.Point(10, 80);
            this.backStageButton2.Name = "backStageButton2";
            this.backStageButton2.Size = new System.Drawing.Size(110, 25);
            this.backStageButton2.TabIndex = 4;
            this.backStageButton2.Text = "Exit";
            // 
            // miniToolBar1
            // 
            this.miniToolBar1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.miniToolBar1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Cut,
            this.Copy});
            this.miniToolBar1.Name = "miniToolBar1";
            this.miniToolBar1.Size = new System.Drawing.Size(50, 31);
            // 
            // Cut
            // 
            this.Cut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Cut.Image = global::WindowsFormsApplication626.Properties.Resources.Cut16;
            this.Cut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Cut.Name = "Cut";
            this.Cut.Size = new System.Drawing.Size(24, 24);
            this.Cut.Text = "Cut";
            this.Cut.ToolTipText = "Cut";
            // 
            // Copy
            // 
            this.Copy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Copy.Image = global::WindowsFormsApplication626.Properties.Resources.Copy16;
            this.Copy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Copy.Name = "Copy";
            this.Copy.Size = new System.Drawing.Size(24, 24);
            this.Copy.Text = "Copy";
            // 
            // FormDemo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1152, 794);
            this.Controls.Add(this.Print);
            this.Controls.Add(this.dockingClientPanel1);
            this.Controls.Add(this.ribbonControlAdv1);
            this.Controls.Add(this.New);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormDemo";
            this.Text = "FormDemo";
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControlAdv1)).EndInit();
            this.ribbonControlAdv1.ResumeLayout(false);
            this.ribbonControlAdv1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Print)).EndInit();
            this.Print.ResumeLayout(false);
            this.toolStripTabItem1.Panel.ResumeLayout(false);
            this.toolStripEx1.ResumeLayout(false);
            this.toolStripEx1.PerformLayout();
            this.toolStripEx2.ResumeLayout(false);
            this.toolStripEx2.PerformLayout();
            this.toolStripEx3.ResumeLayout(false);
            this.toolStripEx3.PerformLayout();
            this.View.Panel.ResumeLayout(false);
            this.toolStripEx4.ResumeLayout(false);
            this.toolStripEx4.PerformLayout();
            this.dockingClientPanel1.ResumeLayout(false);
            this.Output.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dockingManager1)).EndInit();
            this.miniToolBar1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Syncfusion.Windows.Forms.Tools.RibbonControlAdv ribbonControlAdv1;
        private Syncfusion.Windows.Forms.Tools.ToolStripTabItem toolStripTabItem1;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx1;
        private Syncfusion.Windows.Forms.Tools.DockingClientPanel dockingClientPanel1;
        private Syncfusion.Windows.Forms.Tools.DockingManager dockingManager1;
        private System.Windows.Forms.Panel New;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx2;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx3;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem saveASToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private Syncfusion.Windows.Forms.Tools.ToolStripTabItem toolStripTabItem2;
        private System.Windows.Forms.Panel Properties;
        private System.Windows.Forms.Panel Output;
        private System.Windows.Forms.ListBox listBox1;
        private Syncfusion.Windows.Forms.Tools.ToolStripPanelItem toolStripPanelItem1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private Syncfusion.Windows.Forms.Tools.ToolStripPanelItem toolStripPanelItem3;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private Syncfusion.Windows.Forms.Tools.ToolStripPanelItem toolStripPanelItem2;
        private Syncfusion.Windows.Forms.Tools.ToolStripComboBoxEx name;
        private Syncfusion.Windows.Forms.Tools.ToolStripComboBoxEx Subtask;
        private Syncfusion.Windows.Forms.BackStageView backStageView1;
        private Syncfusion.Windows.Forms.BackStageButton backStageButton2;
        private Syncfusion.Windows.Forms.BackStage Print;
        private Syncfusion.Windows.Forms.BackStageButton backStageButton1;
        private Syncfusion.Windows.Forms.BackStageButton backStageButton3;
        private Syncfusion.Windows.Forms.BackStageTab backStageTab1;
        private Syncfusion.Windows.Forms.Tools.ToolStripGallery toolStripGallery1;
        private Syncfusion.Windows.Forms.Tools.ToolStripTabItem View;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx4;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private Syncfusion.Windows.Forms.Tools.MiniToolBar miniToolBar1;
        private System.Windows.Forms.ToolStripButton Cut;
        private System.Windows.Forms.ToolStripButton Copy;
    }
}