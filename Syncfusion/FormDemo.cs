﻿using Syncfusion.Windows.Forms.Tools;
using System;
using System.Windows.Forms;

namespace WindowsFormsApplication626
{
    public partial class FormDemo : RibbonForm
    {
        public FormDemo()
        {
            InitializeComponent();
            this.ribbonControlAdv1.RibbonStyle = RibbonStyle.Office2013;

            this.dockingManager1.DockControl(this.New, this, Syncfusion.Windows.Forms.Tools.DockingStyle.Left, 100);
            this.dockingManager1.DockControl(this.Properties, this, Syncfusion.Windows.Forms.Tools.DockingStyle.Right, 100);
            this.dockingManager1.DockControl(this.Output, this, Syncfusion.Windows.Forms.Tools.DockingStyle.Bottom, 200);

            this.ribbonControlAdv1.MenuButtonClick += new EventHandler(ribbonControlAdv1_MenuButtonClick);

        }


        void ribbonControlAdv1_MenuButtonClick(object sender, EventArgs e)
        {
            if (!this.ribbonControlAdv1.BackStageView.IsVisible && ribbonControlAdv1.RibbonStyle == RibbonStyle.Office2013) { }
        }

        private void listBox1_Click(object sender, EventArgs e)
        {
            if (miniToolBar1 !=null)
            {
                miniToolBar1.Show(listBox1,0,0);
            }
        }
    }
}
