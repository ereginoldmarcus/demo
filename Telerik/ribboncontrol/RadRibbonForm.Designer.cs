﻿namespace ribboncontrol
{
    partial class RadRibbonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem12 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem13 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem14 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem15 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem16 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem17 = new Telerik.WinControls.UI.RadListDataItem();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radDock1 = new Telerik.WinControls.UI.Docking.RadDock();
            this.toolWindow1 = new Telerik.WinControls.UI.Docking.ToolWindow();
            this.radSplitContainer2 = new Telerik.WinControls.UI.RadSplitContainer();
            this.toolTabStrip2 = new Telerik.WinControls.UI.Docking.ToolTabStrip();
            this.toolWindow2 = new Telerik.WinControls.UI.Docking.ToolWindow();
            this.radRichTextEditor1 = new Telerik.WinControls.UI.RadRichTextEditor();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.toolTabStrip1 = new Telerik.WinControls.UI.Docking.ToolTabStrip();
            this.documentContainer1 = new Telerik.WinControls.UI.Docking.DocumentContainer();
            this.toolTabStrip3 = new Telerik.WinControls.UI.Docking.ToolTabStrip();
            this.toolWindow3 = new Telerik.WinControls.UI.Docking.ToolWindow();
            this.radButtonElement1 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement5 = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarButtonGroup1 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radButtonElement3 = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup4 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radButtonElement12 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement13 = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarButtonGroup3 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.ribbonTab4 = new Telerik.WinControls.UI.RibbonTab();
            this.radRibbonBar1 = new Telerik.WinControls.UI.RadRibbonBar();
            this.radRibbonBarBackstageView1 = new Telerik.WinControls.UI.RadRibbonBarBackstageView();
            this.backstageButtonItem1 = new Telerik.WinControls.UI.BackstageButtonItem();
            this.backstageButtonItem2 = new Telerik.WinControls.UI.BackstageButtonItem();
            this.backstageButtonItem3 = new Telerik.WinControls.UI.BackstageButtonItem();
            this.backstageButtonItem4 = new Telerik.WinControls.UI.BackstageButtonItem();
            this.backstageButtonItem5 = new Telerik.WinControls.UI.BackstageButtonItem();
            this.ribbonTab1 = new Telerik.WinControls.UI.RibbonTab();
            this.radRibbonBarGroup1 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radButtonElement4 = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarButtonGroup2 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radButtonElement11 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement10 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement15 = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup2 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radButtonElement6 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement7 = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup5 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarGroup6 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radDropDownButtonElement1 = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.radMenuItem1 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem2 = new Telerik.WinControls.UI.RadMenuItem();
            this.ribbonTab5 = new Telerik.WinControls.UI.RibbonTab();
            this.ribbonTab2 = new Telerik.WinControls.UI.RibbonTab();
            this.contextualTabGroup1 = new Telerik.WinControls.UI.ContextualTabGroup();
            this.ribbonTab3 = new Telerik.WinControls.UI.RibbonTab();
            this.radRibbonBarGroup3 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radButtonElement8 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement9 = new Telerik.WinControls.UI.RadButtonElement();
            this.radMenuItem3 = new Telerik.WinControls.UI.RadMenuItem();
            this.radButtonElement2 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement14 = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarButtonGroup5 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radRibbonBarButtonGroup4 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radLabelElement1 = new Telerik.WinControls.UI.RadLabelElement();
            this.radDropDownListElement1 = new Telerik.WinControls.UI.RadDropDownListElement();
            this.radRibbonBarButtonGroup6 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radRibbonBarButtonGroup7 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radLabelElement2 = new Telerik.WinControls.UI.RadLabelElement();
            this.radDropDownListElement2 = new Telerik.WinControls.UI.RadDropDownListElement();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDock1)).BeginInit();
            this.radDock1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).BeginInit();
            this.radSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip2)).BeginInit();
            this.toolTabStrip2.SuspendLayout();
            this.toolWindow2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRichTextEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip1)).BeginInit();
            this.toolTabStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.documentContainer1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip3)).BeginInit();
            this.toolTabStrip3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRibbonBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRibbonBarBackstageView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownListElement1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownListElement2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 501);
            this.radStatusStrip1.Margin = new System.Windows.Forms.Padding(4);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(640, 30);
            this.radStatusStrip1.SizingGrip = false;
            this.radStatusStrip1.TabIndex = 1;
            this.radStatusStrip1.Text = "radStatusStrip1";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radDock1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 165);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(640, 336);
            this.panel1.TabIndex = 2;
            // 
            // radDock1
            // 
            this.radDock1.ActiveWindow = this.toolWindow2;
            this.radDock1.Controls.Add(this.radSplitContainer2);
            this.radDock1.Controls.Add(this.toolTabStrip3);
            this.radDock1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radDock1.IsCleanUpTarget = true;
            this.radDock1.Location = new System.Drawing.Point(0, 0);
            this.radDock1.MainDocumentContainer = this.documentContainer1;
            this.radDock1.Name = "radDock1";
            // 
            // 
            // 
            this.radDock1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radDock1.Size = new System.Drawing.Size(640, 336);
            this.radDock1.TabIndex = 1;
            this.radDock1.TabStop = false;
            this.radDock1.Text = "radDock1";
            // 
            // toolWindow1
            // 
            this.toolWindow1.Caption = null;
            this.toolWindow1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolWindow1.Location = new System.Drawing.Point(1, 26);
            this.toolWindow1.Name = "toolWindow1";
            this.toolWindow1.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.Docked;
            this.toolWindow1.Size = new System.Drawing.Size(198, 94);
            this.toolWindow1.Text = "toolWindow1";
            // 
            // radSplitContainer2
            // 
            this.radSplitContainer2.Controls.Add(this.toolTabStrip2);
            this.radSplitContainer2.Controls.Add(this.radSplitContainer1);
            this.radSplitContainer2.IsCleanUpTarget = true;
            this.radSplitContainer2.Location = new System.Drawing.Point(5, 5);
            this.radSplitContainer2.Name = "radSplitContainer2";
            this.radSplitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.radSplitContainer2.Padding = new System.Windows.Forms.Padding(5);
            // 
            // 
            // 
            this.radSplitContainer2.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radSplitContainer2.Size = new System.Drawing.Size(426, 326);
            this.radSplitContainer2.TabIndex = 0;
            this.radSplitContainer2.TabStop = false;
            // 
            // toolTabStrip2
            // 
            this.toolTabStrip2.CanUpdateChildIndex = true;
            this.toolTabStrip2.Controls.Add(this.toolWindow2);
            this.toolTabStrip2.Location = new System.Drawing.Point(0, 0);
            this.toolTabStrip2.Name = "toolTabStrip2";
            // 
            // 
            // 
            this.toolTabStrip2.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.toolTabStrip2.SelectedIndex = 0;
            this.toolTabStrip2.Size = new System.Drawing.Size(426, 200);
            this.toolTabStrip2.TabIndex = 1;
            this.toolTabStrip2.TabStop = false;
            // 
            // toolWindow2
            // 
            this.toolWindow2.Caption = null;
            this.toolWindow2.Controls.Add(this.radRichTextEditor1);
            this.toolWindow2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolWindow2.Location = new System.Drawing.Point(1, 26);
            this.toolWindow2.Name = "toolWindow2";
            this.toolWindow2.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.Docked;
            this.toolWindow2.Size = new System.Drawing.Size(424, 172);
            this.toolWindow2.Text = "toolWindow2";
            // 
            // radRichTextEditor1
            // 
            this.radRichTextEditor1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(189)))), ((int)(((byte)(232)))));
            this.radRichTextEditor1.CaretWidth = float.NaN;
            this.radRichTextEditor1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radRichTextEditor1.Location = new System.Drawing.Point(0, 0);
            this.radRichTextEditor1.Name = "radRichTextEditor1";
            this.radRichTextEditor1.SelectionFill = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(78)))), ((int)(((byte)(158)))), ((int)(((byte)(255)))));
            this.radRichTextEditor1.Size = new System.Drawing.Size(424, 172);
            this.radRichTextEditor1.TabIndex = 0;
            this.radRichTextEditor1.Text = "test";
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.toolTabStrip1);
            this.radSplitContainer1.Controls.Add(this.documentContainer1);
            this.radSplitContainer1.IsCleanUpTarget = true;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 204);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Padding = new System.Windows.Forms.Padding(5);
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radSplitContainer1.Size = new System.Drawing.Size(426, 122);
            this.radSplitContainer1.TabIndex = 0;
            this.radSplitContainer1.TabStop = false;
            // 
            // toolTabStrip1
            // 
            this.toolTabStrip1.CanUpdateChildIndex = true;
            this.toolTabStrip1.Controls.Add(this.toolWindow1);
            this.toolTabStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolTabStrip1.Name = "toolTabStrip1";
            // 
            // 
            // 
            this.toolTabStrip1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.toolTabStrip1.SelectedIndex = 0;
            this.toolTabStrip1.Size = new System.Drawing.Size(200, 122);
            this.toolTabStrip1.TabIndex = 1;
            this.toolTabStrip1.TabStop = false;
            // 
            // documentContainer1
            // 
            this.documentContainer1.Name = "documentContainer1";
            // 
            // 
            // 
            this.documentContainer1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.documentContainer1.SizeInfo.SizeMode = Telerik.WinControls.UI.Docking.SplitPanelSizeMode.Fill;
            this.documentContainer1.TabIndex = 2;
            // 
            // toolTabStrip3
            // 
            this.toolTabStrip3.CanUpdateChildIndex = true;
            this.toolTabStrip3.Controls.Add(this.toolWindow3);
            this.toolTabStrip3.Location = new System.Drawing.Point(435, 5);
            this.toolTabStrip3.Name = "toolTabStrip3";
            // 
            // 
            // 
            this.toolTabStrip3.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.toolTabStrip3.SelectedIndex = 0;
            this.toolTabStrip3.Size = new System.Drawing.Size(200, 326);
            this.toolTabStrip3.TabIndex = 1;
            this.toolTabStrip3.TabStop = false;
            // 
            // toolWindow3
            // 
            this.toolWindow3.Caption = null;
            this.toolWindow3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolWindow3.Location = new System.Drawing.Point(1, 26);
            this.toolWindow3.Name = "toolWindow3";
            this.toolWindow3.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.Docked;
            this.toolWindow3.Size = new System.Drawing.Size(198, 298);
            this.toolWindow3.Text = "toolWindow3";
            // 
            // radButtonElement1
            // 
            this.radButtonElement1.Name = "radButtonElement1";
            this.radButtonElement1.Text = "Select All";
            // 
            // radButtonElement5
            // 
            this.radButtonElement5.Name = "radButtonElement5";
            this.radButtonElement5.Text = "Select All";
            // 
            // radRibbonBarButtonGroup1
            // 
            this.radRibbonBarButtonGroup1.Name = "radRibbonBarButtonGroup1";
            this.radRibbonBarButtonGroup1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // radButtonElement3
            // 
            this.radButtonElement3.Name = "radButtonElement3";
            // 
            // radRibbonBarGroup4
            // 
            this.radRibbonBarGroup4.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement12,
            this.radButtonElement13});
            this.radRibbonBarGroup4.Margin = new System.Windows.Forms.Padding(0);
            this.radRibbonBarGroup4.MaxSize = new System.Drawing.Size(0, 0);
            this.radRibbonBarGroup4.MinSize = new System.Drawing.Size(0, 0);
            this.radRibbonBarGroup4.Name = "radRibbonBarGroup4";
            this.radRibbonBarGroup4.Text = "Navigation";
            // 
            // radButtonElement12
            // 
            this.radButtonElement12.Image = global::ribboncontrol.Properties.Resources.select;
            this.radButtonElement12.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement12.Name = "radButtonElement12";
            this.radButtonElement12.Text = "Selection";
            this.radButtonElement12.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // radButtonElement13
            // 
            this.radButtonElement13.Image = global::ribboncontrol.Properties.Resources.resize;
            this.radButtonElement13.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement13.Name = "radButtonElement13";
            this.radButtonElement13.Text = "Window";
            this.radButtonElement13.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // radRibbonBarButtonGroup3
            // 
            this.radRibbonBarButtonGroup3.Name = "radRibbonBarButtonGroup3";
            // 
            // ribbonTab4
            // 
            this.ribbonTab4.AutoEllipsis = false;
            this.ribbonTab4.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ribbonTab4.IsSelected = true;
            this.ribbonTab4.Name = "ribbonTab4";
            this.ribbonTab4.Text = "Construction";
            this.ribbonTab4.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // radRibbonBar1
            // 
            this.radRibbonBar1.ApplicationMenuStyle = Telerik.WinControls.UI.ApplicationMenuStyle.BackstageView;
            this.radRibbonBar1.BackstageControl = this.radRibbonBarBackstageView1;
            this.radRibbonBar1.CommandTabs.AddRange(new Telerik.WinControls.RadItem[] {
            this.ribbonTab1,
            this.ribbonTab5,
            this.ribbonTab2,
            this.ribbonTab3});
            this.radRibbonBar1.ContextualTabGroups.AddRange(new Telerik.WinControls.RadItem[] {
            this.contextualTabGroup1});
            // 
            // 
            // 
            this.radRibbonBar1.ExitButton.Text = "Exit";
            this.radRibbonBar1.ExitButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radRibbonBar1.Location = new System.Drawing.Point(0, 0);
            this.radRibbonBar1.Margin = new System.Windows.Forms.Padding(4);
            this.radRibbonBar1.Name = "radRibbonBar1";
            // 
            // 
            // 
            this.radRibbonBar1.OptionsButton.Text = "Options";
            this.radRibbonBar1.OptionsButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // 
            // 
            this.radRibbonBar1.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radRibbonBar1.Size = new System.Drawing.Size(640, 165);
            this.radRibbonBar1.StartButtonImage = global::ribboncontrol.Properties.Resources.Root;
            this.radRibbonBar1.StartMenuItems.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem3});
            this.radRibbonBar1.TabIndex = 0;
            this.radRibbonBar1.Text = "RadRibbonForm";
            // 
            // radRibbonBarBackstageView1
            // 
            this.radRibbonBarBackstageView1.EnableKeyMap = true;
            this.radRibbonBarBackstageView1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.backstageButtonItem1,
            this.backstageButtonItem2,
            this.backstageButtonItem3,
            this.backstageButtonItem4,
            this.backstageButtonItem5});
            this.radRibbonBarBackstageView1.Location = new System.Drawing.Point(0, 58);
            this.radRibbonBarBackstageView1.Margin = new System.Windows.Forms.Padding(4);
            this.radRibbonBarBackstageView1.Name = "radRibbonBarBackstageView1";
            this.radRibbonBarBackstageView1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radRibbonBarBackstageView1.SelectedItem = null;
            this.radRibbonBarBackstageView1.Size = new System.Drawing.Size(640, 473);
            this.radRibbonBarBackstageView1.TabIndex = 3;
            // 
            // backstageButtonItem1
            // 
            this.backstageButtonItem1.Image = global::ribboncontrol.Properties.Resources._new;
            this.backstageButtonItem1.Name = "backstageButtonItem1";
            this.backstageButtonItem1.Text = "New";
            // 
            // backstageButtonItem2
            // 
            this.backstageButtonItem2.Image = global::ribboncontrol.Properties.Resources.open;
            this.backstageButtonItem2.Name = "backstageButtonItem2";
            this.backstageButtonItem2.Text = "Open";
            // 
            // backstageButtonItem3
            // 
            this.backstageButtonItem3.Image = global::ribboncontrol.Properties.Resources.save;
            this.backstageButtonItem3.Name = "backstageButtonItem3";
            this.backstageButtonItem3.Text = "Save";
            // 
            // backstageButtonItem4
            // 
            this.backstageButtonItem4.Image = global::ribboncontrol.Properties.Resources.save_as1;
            this.backstageButtonItem4.Name = "backstageButtonItem4";
            this.backstageButtonItem4.Text = "Save As";
            // 
            // backstageButtonItem5
            // 
            this.backstageButtonItem5.Image = global::ribboncontrol.Properties.Resources.print;
            this.backstageButtonItem5.Name = "backstageButtonItem5";
            this.backstageButtonItem5.Text = "Print";
            // 
            // ribbonTab1
            // 
            this.ribbonTab1.IsSelected = true;
            this.ribbonTab1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarGroup1,
            this.radRibbonBarGroup2,
            this.radRibbonBarGroup5,
            this.radRibbonBarGroup6});
            this.ribbonTab1.Name = "ribbonTab1";
            this.ribbonTab1.Text = "Home";
            // 
            // radRibbonBarGroup1
            // 
            this.radRibbonBarGroup1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement4,
            this.radRibbonBarButtonGroup2});
            this.radRibbonBarGroup1.Name = "radRibbonBarGroup1";
            this.radRibbonBarGroup1.Text = "Buttons";
            // 
            // radButtonElement4
            // 
            this.radButtonElement4.Image = global::ribboncontrol.Properties.Resources.new_paste_32;
            this.radButtonElement4.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement4.Name = "radButtonElement4";
            this.radButtonElement4.Text = "Paste";
            this.radButtonElement4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // radRibbonBarButtonGroup2
            // 
            this.radRibbonBarButtonGroup2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement11,
            this.radButtonElement10,
            this.radButtonElement15});
            this.radRibbonBarButtonGroup2.Name = "radRibbonBarButtonGroup2";
            this.radRibbonBarButtonGroup2.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarButtonGroup2.Text = "cut-copy";
            // 
            // radButtonElement11
            // 
            this.radButtonElement11.Image = global::ribboncontrol.Properties.Resources.Spread_Cut;
            this.radButtonElement11.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButtonElement11.ImageIndex = -1;
            this.radButtonElement11.Name = "radButtonElement11";
            this.radButtonElement11.ShowBorder = false;
            this.radButtonElement11.Text = "Cut";
            this.radButtonElement11.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // radButtonElement10
            // 
            this.radButtonElement10.Image = global::ribboncontrol.Properties.Resources.Spread_Copy;
            this.radButtonElement10.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButtonElement10.ImageIndex = 0;
            this.radButtonElement10.Name = "radButtonElement10";
            this.radButtonElement10.ShowBorder = false;
            this.radButtonElement10.Text = "Copy";
            this.radButtonElement10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // radButtonElement15
            // 
            this.radButtonElement15.Image = global::ribboncontrol.Properties.Resources.paste;
            this.radButtonElement15.ImageAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radButtonElement15.ImageIndex = -1;
            this.radButtonElement15.Name = "radButtonElement15";
            this.radButtonElement15.ShowBorder = false;
            this.radButtonElement15.Text = "Paste";
            this.radButtonElement15.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // radRibbonBarGroup2
            // 
            this.radRibbonBarGroup2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement6,
            this.radButtonElement7});
            this.radRibbonBarGroup2.Name = "radRibbonBarGroup2";
            this.radRibbonBarGroup2.Text = "Buttons";
            // 
            // radButtonElement6
            // 
            this.radButtonElement6.Image = global::ribboncontrol.Properties.Resources.select;
            this.radButtonElement6.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement6.Name = "radButtonElement6";
            this.radButtonElement6.Text = "Selection";
            this.radButtonElement6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // radButtonElement7
            // 
            this.radButtonElement7.Image = global::ribboncontrol.Properties.Resources.resize;
            this.radButtonElement7.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement7.Name = "radButtonElement7";
            this.radButtonElement7.Text = "Window";
            this.radButtonElement7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // radRibbonBarGroup5
            // 
            this.radRibbonBarGroup5.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup4,
            this.radRibbonBarButtonGroup7});
            this.radRibbonBarGroup5.Name = "radRibbonBarGroup5";
            this.radRibbonBarGroup5.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarGroup5.Text = "Dropdown list";
            this.radRibbonBarGroup5.TextOrientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // radRibbonBarGroup6
            // 
            this.radRibbonBarGroup6.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radDropDownButtonElement1});
            this.radRibbonBarGroup6.Name = "radRibbonBarGroup6";
            this.radRibbonBarGroup6.Text = "Dropdown Buttons";
            // 
            // radDropDownButtonElement1
            // 
            this.radDropDownButtonElement1.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.radDropDownButtonElement1.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.radDropDownButtonElement1.ExpandArrowButton = false;
            this.radDropDownButtonElement1.Image = global::ribboncontrol.Properties.Resources.Search2;
            this.radDropDownButtonElement1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem1,
            this.radMenuItem2});
            this.radDropDownButtonElement1.Name = "radDropDownButtonElement1";
            this.radDropDownButtonElement1.Text = "Visible";
            this.radDropDownButtonElement1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // radMenuItem1
            // 
            this.radMenuItem1.Image = global::ribboncontrol.Properties.Resources._1RibbonMenuNew2;
            this.radMenuItem1.Name = "radMenuItem1";
            this.radMenuItem1.Text = "Model";
            // 
            // radMenuItem2
            // 
            this.radMenuItem2.Image = global::ribboncontrol.Properties.Resources.gridview;
            this.radMenuItem2.Name = "radMenuItem2";
            this.radMenuItem2.Text = "Visible";
            // 
            // ribbonTab5
            // 
            this.ribbonTab5.Name = "ribbonTab5";
            this.ribbonTab5.Text = "Construction";
            // 
            // ribbonTab2
            // 
            this.ribbonTab2.ContextualTabGroup = this.contextualTabGroup1;
            this.ribbonTab2.IsSelected = false;
            this.ribbonTab2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarGroup3});
            this.ribbonTab2.Name = "ribbonTab2";
            this.ribbonTab2.Text = "Insert";
            // 
            // contextualTabGroup1
            // 
            this.contextualTabGroup1.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(235)))), ((int)(((byte)(219)))), ((int)(((byte)(130)))));
            this.contextualTabGroup1.Name = "contextualTabGroup1";
            this.contextualTabGroup1.TabItems.AddRange(new Telerik.WinControls.RadItem[] {
            this.ribbonTab2,
            this.ribbonTab3});
            this.contextualTabGroup1.Text = "Task";
            // 
            // ribbonTab3
            // 
            this.ribbonTab3.ContextualTabGroup = this.contextualTabGroup1;
            this.ribbonTab3.IsSelected = false;
            this.ribbonTab3.Name = "ribbonTab3";
            this.ribbonTab3.Text = "Modeling";
            // 
            // radRibbonBarGroup3
            // 
            this.radRibbonBarGroup3.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement8,
            this.radButtonElement9});
            this.radRibbonBarGroup3.Name = "radRibbonBarGroup3";
            this.radRibbonBarGroup3.Text = "Import";
            // 
            // radButtonElement8
            // 
            this.radButtonElement8.Image = global::ribboncontrol.Properties.Resources.issues;
            this.radButtonElement8.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButtonElement8.Name = "radButtonElement8";
            this.radButtonElement8.Text = "Import Files";
            this.radButtonElement8.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButtonElement8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // radButtonElement9
            // 
            this.radButtonElement9.Image = global::ribboncontrol.Properties.Resources.pdf_doc_icon2;
            this.radButtonElement9.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.radButtonElement9.Name = "radButtonElement9";
            this.radButtonElement9.Text = "Attatchments";
            this.radButtonElement9.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButtonElement9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // radMenuItem3
            // 
            this.radMenuItem3.Name = "radMenuItem3";
            this.radMenuItem3.Text = "radMenuItem3";
            // 
            // radButtonElement2
            // 
            this.radButtonElement2.Image = global::ribboncontrol.Properties.Resources.Spread_Cut;
            this.radButtonElement2.Name = "radButtonElement2";
            this.radButtonElement2.Text = "Cut";
            this.radButtonElement2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // radButtonElement14
            // 
            this.radButtonElement14.Image = global::ribboncontrol.Properties.Resources.Spread_Copy;
            this.radButtonElement14.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButtonElement14.Name = "radButtonElement14";
            this.radButtonElement14.Text = "Copy";
            this.radButtonElement14.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // radRibbonBarButtonGroup5
            // 
            this.radRibbonBarButtonGroup5.Name = "radRibbonBarButtonGroup5";
            this.radRibbonBarButtonGroup5.Text = "radRibbonBarButtonGroup5";
            this.radRibbonBarButtonGroup5.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // radRibbonBarButtonGroup4
            // 
            this.radRibbonBarButtonGroup4.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radLabelElement1,
            this.radDropDownListElement1});
            this.radRibbonBarButtonGroup4.Name = "radRibbonBarButtonGroup4";
            this.radRibbonBarButtonGroup4.Text = "radRibbonBarButtonGroup4";
            // 
            // radLabelElement1
            // 
            this.radLabelElement1.Name = "radLabelElement1";
            this.radLabelElement1.Text = "Font";
            this.radLabelElement1.TextWrap = true;
            // 
            // radDropDownListElement1
            // 
            this.radDropDownListElement1.ArrowButtonMinWidth = 17;
            this.radDropDownListElement1.AutoCompleteAppend = null;
            this.radDropDownListElement1.AutoCompleteDataSource = null;
            this.radDropDownListElement1.AutoCompleteSuggest = null;
            this.radDropDownListElement1.DataMember = "";
            this.radDropDownListElement1.DataSource = null;
            this.radDropDownListElement1.DefaultValue = null;
            this.radDropDownListElement1.DisplayMember = "";
            this.radDropDownListElement1.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InQuad;
            this.radDropDownListElement1.DropDownAnimationEnabled = true;
            this.radDropDownListElement1.EditableElementText = "Calibri";
            this.radDropDownListElement1.EditorElement = this.radDropDownListElement1;
            this.radDropDownListElement1.EditorManager = null;
            this.radDropDownListElement1.Filter = null;
            this.radDropDownListElement1.FilterExpression = "";
            this.radDropDownListElement1.Focusable = true;
            this.radDropDownListElement1.FormatString = "";
            this.radDropDownListElement1.FormattingEnabled = true;
            this.radDropDownListElement1.ItemHeight = 18;
            radListDataItem12.Text = "Calibri";
            radListDataItem13.Text = "Calibri Light";
            radListDataItem14.Text = "Cambria";
            radListDataItem15.Text = "Candara";
            radListDataItem16.Text = "Centaur";
            this.radDropDownListElement1.Items.Add(radListDataItem12);
            this.radDropDownListElement1.Items.Add(radListDataItem13);
            this.radDropDownListElement1.Items.Add(radListDataItem14);
            this.radDropDownListElement1.Items.Add(radListDataItem15);
            this.radDropDownListElement1.Items.Add(radListDataItem16);
            this.radDropDownListElement1.MaxDropDownItems = 0;
            this.radDropDownListElement1.MaxLength = 32767;
            this.radDropDownListElement1.MaxValue = null;
            this.radDropDownListElement1.MinValue = null;
            this.radDropDownListElement1.Name = "radDropDownListElement1";
            this.radDropDownListElement1.NullValue = null;
            this.radDropDownListElement1.OwnerOffset = 0;
            this.radDropDownListElement1.ShowImageInEditorArea = true;
            this.radDropDownListElement1.SortStyle = Telerik.WinControls.Enumerations.SortStyle.None;
            this.radDropDownListElement1.Value = null;
            this.radDropDownListElement1.ValueMember = "";
            // 
            // radRibbonBarButtonGroup6
            // 
            this.radRibbonBarButtonGroup6.Name = "radRibbonBarButtonGroup6";
            // 
            // radRibbonBarButtonGroup7
            // 
            this.radRibbonBarButtonGroup7.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radLabelElement2,
            this.radDropDownListElement2});
            this.radRibbonBarButtonGroup7.Name = "radRibbonBarButtonGroup7";
            this.radRibbonBarButtonGroup7.Text = "radRibbonBarButtonGroup7";
            // 
            // radLabelElement2
            // 
            this.radLabelElement2.Name = "radLabelElement2";
            this.radLabelElement2.Text = "Size";
            this.radLabelElement2.TextWrap = true;
            // 
            // radDropDownListElement2
            // 
            this.radDropDownListElement2.ArrowButtonMinWidth = 17;
            this.radDropDownListElement2.AutoCompleteAppend = null;
            this.radDropDownListElement2.AutoCompleteDataSource = null;
            this.radDropDownListElement2.AutoCompleteSuggest = null;
            this.radDropDownListElement2.DataMember = "";
            this.radDropDownListElement2.DataSource = null;
            this.radDropDownListElement2.DefaultValue = null;
            this.radDropDownListElement2.DisplayMember = "";
            this.radDropDownListElement2.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InQuad;
            this.radDropDownListElement2.DropDownAnimationEnabled = true;
            this.radDropDownListElement2.EditableElementText = "8";
            this.radDropDownListElement2.EditorElement = this.radDropDownListElement2;
            this.radDropDownListElement2.EditorManager = null;
            this.radDropDownListElement2.Filter = null;
            this.radDropDownListElement2.FilterExpression = "";
            this.radDropDownListElement2.Focusable = true;
            this.radDropDownListElement2.FormatString = "";
            this.radDropDownListElement2.FormattingEnabled = true;
            this.radDropDownListElement2.ItemHeight = 18;
            radListDataItem1.Text = "8";
            radListDataItem2.Text = "9";
            radListDataItem3.Text = "10";
            radListDataItem4.Text = "12";
            radListDataItem5.Text = "14";
            radListDataItem17.Text = "16";
            this.radDropDownListElement2.Items.Add(radListDataItem1);
            this.radDropDownListElement2.Items.Add(radListDataItem2);
            this.radDropDownListElement2.Items.Add(radListDataItem3);
            this.radDropDownListElement2.Items.Add(radListDataItem4);
            this.radDropDownListElement2.Items.Add(radListDataItem5);
            this.radDropDownListElement2.Items.Add(radListDataItem17);
            this.radDropDownListElement2.MaxDropDownItems = 0;
            this.radDropDownListElement2.MaxLength = 32767;
            this.radDropDownListElement2.MaxValue = null;
            this.radDropDownListElement2.MinValue = null;
            this.radDropDownListElement2.Name = "radDropDownListElement2";
            this.radDropDownListElement2.NullValue = null;
            this.radDropDownListElement2.OwnerOffset = 0;
            this.radDropDownListElement2.ShowImageInEditorArea = true;
            this.radDropDownListElement2.SortStyle = Telerik.WinControls.Enumerations.SortStyle.None;
            this.radDropDownListElement2.Value = null;
            this.radDropDownListElement2.ValueMember = "";
            // 
            // RadRibbonForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(640, 531);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.radStatusStrip1);
            this.Controls.Add(this.radRibbonBar1);
            this.Controls.Add(this.radRibbonBarBackstageView1);
            this.MainMenuStrip = null;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "RadRibbonForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "RadRibbonForm";
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radDock1)).EndInit();
            this.radDock1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).EndInit();
            this.radSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip2)).EndInit();
            this.toolTabStrip2.ResumeLayout(false);
            this.toolWindow2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radRichTextEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip1)).EndInit();
            this.toolTabStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.documentContainer1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip3)).EndInit();
            this.toolTabStrip3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radRibbonBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRibbonBarBackstageView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownListElement1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownListElement2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadRibbonBar radRibbonBar1;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RibbonTab ribbonTab1;
        private Telerik.WinControls.UI.RibbonTab ribbonTab2;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup1;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup2;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement4;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement6;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement5;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement7;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup3;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement8;
        private Telerik.WinControls.UI.RibbonTab ribbonTab3;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup2;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement10;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement11;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement2;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement3;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement9;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup5;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup4;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement12;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement13;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup3;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup6;
        private Telerik.WinControls.UI.RadDropDownButtonElement radDropDownButtonElement1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3;
        private Telerik.WinControls.UI.RadRibbonBarBackstageView radRibbonBarBackstageView1;
        private Telerik.WinControls.UI.BackstageButtonItem backstageButtonItem1;
        private Telerik.WinControls.UI.BackstageButtonItem backstageButtonItem2;
        private Telerik.WinControls.UI.BackstageButtonItem backstageButtonItem3;
        private Telerik.WinControls.UI.BackstageButtonItem backstageButtonItem4;
        private Telerik.WinControls.UI.BackstageButtonItem backstageButtonItem5;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement15;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement14;
        private Telerik.WinControls.UI.ContextualTabGroup contextualTabGroup1;
        private Telerik.WinControls.UI.RibbonTab ribbonTab5;
        private Telerik.WinControls.UI.RibbonTab ribbonTab4;
        private Telerik.WinControls.UI.Docking.RadDock radDock1;
        private Telerik.WinControls.UI.Docking.DocumentContainer documentContainer1;
        private Telerik.WinControls.UI.Docking.ToolWindow toolWindow1;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer2;
        private Telerik.WinControls.UI.Docking.ToolTabStrip toolTabStrip2;
        private Telerik.WinControls.UI.Docking.ToolWindow toolWindow2;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.Docking.ToolTabStrip toolTabStrip1;
        private Telerik.WinControls.UI.Docking.ToolTabStrip toolTabStrip3;
        private Telerik.WinControls.UI.Docking.ToolWindow toolWindow3;
        private Telerik.WinControls.UI.RadRichTextEditor radRichTextEditor1;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup5;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup4;
        private Telerik.WinControls.UI.RadLabelElement radLabelElement1;
        private Telerik.WinControls.UI.RadDropDownListElement radDropDownListElement1;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup6;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup7;
        private Telerik.WinControls.UI.RadLabelElement radLabelElement2;
        private Telerik.WinControls.UI.RadDropDownListElement radDropDownListElement2;
    }
}
