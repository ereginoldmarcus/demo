﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InfargisticsDemo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            var child = new ChildForm();
            child.MdiParent = this;
            child.Show();
        }

        private void ultraToolbarsManager1_ToolClick(object sender, Infragistics.Win.UltraWinToolbars.ToolClickEventArgs e)
        {

        }
    }
}
